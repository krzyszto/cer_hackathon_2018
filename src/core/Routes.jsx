import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import ScrollToTop from 'components/App/ScrollToTop/ScrollToTop';
import WelcomeScreen from 'screens/Welcome/Welcome';
import LoginScreen from 'screens/Login/Login';
import FirstLoginScreen from 'screens/FirstLogin/FirstLogin';
import OtherScreen from 'screens/Client/Other/Other';
import OsTelScreen from 'screens/Client/OsTel/OsTel';
import AfterOsTelScreen from 'screens/Client/AfterOsTel/AfterOsTel';
import CategoriesScreen from 'screens/Client/Categories/Categories';
import SubCategoriesScreen from 'screens/Client/SubCategories/SubCategories';
import OfferCompletedScreen from 'screens/Client/OfferCompleted/OfferCompleted';
import OfferListScreen from 'screens/Client/OfferList/OfferList';
import ProfileScreen from 'screens/Client/Profile/Profile';
import CardScreen from 'screens/Client/Card/Card';
import SearchScreen from 'screens/Student/Search/Search';
import ApplyScreen from 'screens/Student/Apply/Apply';
import StatsScreen from 'screens/Student/Stats/Stats';
import MyOffersScreen from 'screens/Student/MyOffers/MyOffers';
import DetailsScreen from 'screens/Student/Details/Details';
import Layout from 'components/App/Layout/Layout';

const wrapInLayout = Screen => props => <Layout><Screen {...props} /></Layout>;

export default() => (
  <BrowserRouter>
    <ScrollToTop>
      <Switch>
        <Route exact path="/" render={wrapInLayout(WelcomeScreen)} />
        <Route exact path="/login" render={wrapInLayout(LoginScreen)} />
        <Route exact path="/first_login" render={wrapInLayout(FirstLoginScreen)} />
        <Route exact path="/student/search" render={wrapInLayout(SearchScreen)} />
        <Route exact path="/student/apply" render={wrapInLayout(ApplyScreen)} />
        <Route exact path="/student/stats" render={wrapInLayout(StatsScreen)} />
        <Route exact path="/student/offers" render={wrapInLayout(MyOffersScreen)} />
        <Route exact path="/student/offers/details" render={wrapInLayout(DetailsScreen)} />
        <Route exact path="/client/categories" render={wrapInLayout(CategoriesScreen)} />
        <Route exact path="/client/subcategories" render={wrapInLayout(SubCategoriesScreen)} />
        <Route exact path="/client/offers" render={wrapInLayout(OfferListScreen)} />
        <Route exact path="/client/profile" render={wrapInLayout(ProfileScreen)} />
        <Route exact path="/client/card" render={wrapInLayout(CardScreen)} />
        <Route exact path="/client/offers/completed" render={wrapInLayout(OfferCompletedScreen)} />
        <Route exact path="/client/offers/create/other" render={wrapInLayout(OtherScreen)} />
        <Route exact path="/client/offers/create/ostel" render={wrapInLayout(OsTelScreen)} />
        <Route exact path="/client/offers/create/afterostel" render={wrapInLayout(AfterOsTelScreen)} />
      </Switch>
    </ScrollToTop>
  </BrowserRouter>
);
