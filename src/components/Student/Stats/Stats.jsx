import React from 'react';
import { Link } from 'react-router-dom';
import './Stats.css';

const Stats = () => (
  <div className="stats__container">
    <h1 className="h2__text2">Twoje statystyki</h1>
    <div className="stats__wraper2">
      <div className="stats__item">
        <label className="stats__label">Komputery:</label><span className="stats__amount">120h</span>
      </div>
      <div className="stats__item">
        <label className="stats__label">Strony www:</label><span className="stats__amount">40h</span>
      </div>
      <div className="stats__item">
        <label className="stats__label">Telefony:</label><span className="stats__amount">0h</span>
      </div>
      <div className="stats__item">
        <label className="stats__label">RTV:</label><span className="stats__amount">40h</span>
      </div>
      <div className="stats__item">
        <label className="stats__label">Internet:</label><span className="stats__amount">40h</span>
      </div>
      <div className="stats__item">
        <label className="stats__label">AGD:</label><span className="stats__amount">40h</span>
      </div>
    </div>
    <h3 className="h3__text">Średnia ocena</h3>
    <div className="star__wrapper">
      <img src="/assets/star.png" alt="star" className="star__icon" />
      <img src="/assets/star.png" alt="star" className="star__icon" />
      <img src="/assets/star.png" alt="star" className="star__icon" />
      <img src="/assets/star.png" alt="star" className="star__icon" />
      <img src="/assets/star.png" alt="star" className="star__icon" />
    </div>
    <div className="input__wrapper">
      <Link href="/student/search" to="/student/search" className="profile__link">Wróć</Link>
    </div>
  </div>
);

export default Stats;
