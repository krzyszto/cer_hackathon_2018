import React from 'react';
import { Link } from 'react-router-dom';
import './Details.css';

const Details = () => (
  <div className="details__container">
    <h1 className="h2__text">Szczegóły zlecenia</h1>
    <div className="details__item">
      <img src="/assets/komputer.png" alt="computer" className="details__icon" /><span className="details__text">Komputery</span>
    </div>
    <div className="details__item">
      <img src="/assets/dom.png" alt="home" className="details__icon" /><span className="details__text">Wizyta Domowa</span>
    </div>
    <div className="details__detail">
      <h3>Kontakt:</h3>
      <span className="details__text">8:00 - 14:00</span>
    </div>
    <div className="details__detail">
      <h3>Cena:</h3>
      <span className="details__text">50 zł</span>
    </div>

    <div className="details__detail">
      <h3>Telefon:</h3>
      <span className="details__text">XXX XXX XXX</span>
    </div>
    <div className="details__detail">
      <h3>Adres:</h3>
      <span className="details__text">Potulicka 6/33 Szczecin</span>
    </div>

    <div className="details__back">
      <Link href="/student/offers" to="/student/offers" className="button__back">
        <span className="details__text">Wróć</span>
      </Link>
    </div>
  </div>
);

export default Details;
