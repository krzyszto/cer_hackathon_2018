import React from 'react';
import { Link } from 'react-router-dom';
import './MyOffers.css';

const MyOffers = props => (
  <div className="offer-wrapper">
    <h1 className="offer__header">Lista moich zgłoszeń</h1>
    <div className="offer-list__wrapper">
      <ul className="offer-result__list">
        {props.offers.map(offer => (
          <li key={offer.id} className="my-offers-result__item">
            <Link href="/student/offers/details" to="/student/offers/details" className="button__result">
              <span className="category__wrapper"><img src={`/assets/${offer.category}.png`} alt="" className="result__icon" /><span>{offer.category}</span></span>
              <span className="offer-subcategory__text">{offer.subcategory}</span>
              <button className="result__button">Szczegóły</button>
            </Link>
          </li>))}
      </ul>
      <div className="my-offer-link__wrapper">
        <Link href="/student/search" to="/student/search" className="my-offer__button">Wróć</Link>
      </div>
    </div>

  </div>
);

export default MyOffers;
