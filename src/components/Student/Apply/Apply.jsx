import React from 'react';
import { Link } from 'react-router-dom';
import './Apply.css';

const Apply = () => (
  <div className="apply__container">
    <h1 className="h2__text">Oferta</h1>
    <div className="apply__item">
      <img src="/assets/komputer.png" alt="computer" className="apply__icon" /><span className="apply__text">Komputer</span>
    </div>
    <div className="apply__item">
      <img src="/assets/dom.png" alt="home" className="apply__icon" /><span className="apply__text">Wizyta Domowa</span>
    </div>
    <div className="apply__one-item">
      <span className="apply__text">Wolno działa</span>
    </div>
    <div className="apply__detail">
      <h3>Kontakt:</h3>
      <span className="apply__text">8:00 - 14:00</span>
    </div>
    <div className="apply__detail">
      <h3>Cena:</h3>
      <span className="apply__text">50 zł</span>
    </div>
    <div className="apply__button">
      <Link href="/student/offers/details" to="/student/offers/details" className="button__apply">
        <span className="apply__text">Akceptuj</span>
      </Link>
    </div>
    <div className="apply__button">
      <Link href="/student/search" to="/student/search" className="button__apply">
        <span className="apply__text">Wróć</span>
      </Link>
    </div>
  </div>
);

export default Apply;
