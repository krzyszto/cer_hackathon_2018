import React from 'react';
import { Link } from 'react-router-dom';
import './Search.css';

const Search = props => (
  <div className="search">
    <h1 className="h1__text">Oferty</h1>
    <div className="search__wrapper">
      <input type="text" name="search" required placeholder="Wyszukaj" className="search__input" />
      <Link href="/student/search" to="/student/search" className="button__login"><img src="/assets/search_lens.png" alt="search icon" className="search__icon" /></Link>
    </div>
    <div className="select__wrapper">
      <select name="text" defaultValue="0" className="select__item" onChange={props.onChange}>
        <option value="0" disabled>Kategorie</option>
        <option value="1">Komputer</option>
        <option value="2">Strony www</option>
        <option value="3">Telefon</option>
        <option value="4">RTV</option>
        <option value="5">Internet</option>
        <option value="6">AGD</option>
      </select>
    </div>
    <div className="offers__wrapper">
      <ul className="offers-results__list">
        {props.offers.map(offer => (
          <li key={offer.id} className="offers-results__item">
            <Link href="/student/apply" to="/student/apply" className="button__results">
              <span className="category__wrapper"><img src={`/assets/${offer.category}.png`} alt="" className="results__icon" /><span>{offer.category}</span></span>
              <span className="category__wrapper"><img src={`/assets/${offer.contact}.png`} alt="" className="results__icon" />{offer.contact}</span>
              <span className="subcategory__text">{offer.subcategory}</span>
              <button className="results__button">Szczegóły</button>
            </Link>
          </li>))}
      </ul>
    </div>
  </div>
);

export default Search;
