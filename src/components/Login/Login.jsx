import React from 'react';
import { Link } from 'react-router-dom';
import './Login.css';

const Login = () => (
  <div className="login">
    <h1 className="h1__text">Zaloguj się</h1>
    <div className="input__wrapper">
      <label className="login__label" htmlFor="email">E-mail</label>
      <input type="email" name="email" required placeholder="wpisz e-mail" className="login__input" />
    </div>
    <div className="input__wrapper">
      <label className="login__label" htmlFor="password">Hasło</label>
      <input type="password" name="password" required placeholder="wpisz hasło" className="login__input" />
    </div>
    <div className="input__wrapper">
      <Link href="/first_login" to="/first_login" className="login__link">Zaloguj się</Link>
    </div>
  </div>
);

export default Login;
