import React from 'react';
import { Link } from 'react-router-dom';
import './Welcome.css';

const Welcome = () => (
  <div className="welcome__container">
    <h1 className="h1__text">Zaloguj się</h1>
    <div className="login__item">
      <Link href="/first_login" to="/first_login" className="button__login">
        <img src="assets/facebook_icon.png" alt="facebook icon" className="login__icon" /><span className="button__text">Facebook</span>
      </Link>
    </div>
    <div className="login__item">
      <Link href="/first_login" to="/first_login" className="button__login">
        <img src="assets/google_icon.png" alt="facebook icon" className="login__icon" /><span className="button__text">Gmail</span>
      </Link>
    </div>
    <div className="login__item">
      <Link href="/login" to="/login" className="button__login">
        <img src="assets/account_icon.png" alt="facebook icon" className="login__icon" /><span className="button__text">Konto</span>
      </Link>
    </div>
  </div>
);

export default Welcome;
