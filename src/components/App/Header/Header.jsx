import React from 'react';
import { Link } from 'react-router-dom';
import './Header.css';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menuVisible: false,
    };
    this.toggleClass = this.toggleClass.bind(this);
  }

  toggleClass() {
    this.setState({ menuVisible: !this.state.menuVisible });
  }

  logout() {
    this.props.logout();
    this.setState({ menuVisible: false });
  }

  render() {
    const isVisible = this.state.menuVisible ? 'header__menu--visible' : 'header__menu--hidden';
    return (
      <header>
        <nav>
          <div className="header__logo">
            <Link href="/" to="/" className="header__logo--link">RentMe</Link>
          </div>
          {this.props.student &&
          <div>
            <button className="header__menu" onClick={this.toggleClass}><img src="/assets/menu_hamburger.png" alt="hamburger_menu" className="hamburger__img" /></button>
            <div className={`header__menu--student ${isVisible}`}>
              <div className="header__menu-item">
                <Link href="/" to="/" className="header__link" onClick={this.logout}>Wyloguj</Link>
              </div>
              <div className="header__menu-item">
                <Link href="/student/offers" to="/student/offers" className="header__link">Moje Oferty</Link>
              </div>
              <div className="header__menu-item">
                <Link href="/student/stats" to="/student/stats" className="header__link">Statystyki</Link>
              </div>
            </div>
          </div>}
          {this.props.client &&
          <div>
            <button className="header__menu" onClick={this.toggleClass}><img src="/assets/menu_hamburger.png" alt="hamburger_menu" className="hamburger__img" /></button>
            <div className={`header__menu--client ${isVisible}`}>
              <div className="header__menu-item">
                <Link href="/" to="/" className="header__link" onClick={this.logout}>Wyloguj</Link>
              </div>
            </div>
          </div>}
        </nav>
      </header>
    );
  }
}

export default Header;
