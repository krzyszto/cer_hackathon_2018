import React, { Component } from 'react';
import Header from 'components/App/Header/Header';
import Footer from 'components/App/Footer/Footer';

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        student: undefined,
        client: undefined,
      },
      offers: [
        {
          id: 1,
          category: 'komputer',
          contact: 'telefon',
          subcategory: 'Nie włącza się',
          price: '10zł',
        },
        {
          id: 2,
          category: 'tv',
          contact: 'telefon',
          subcategory: 'Zginęły pliki',
          price: '20zł',
        },
        {
          id: 3,
          category: 'agd',
          contact: 'dom',
          subcategory: 'Wolno działa',
          price: '20zł',
        },
      ],
      newCategory: undefined,
      newContact: undefined,
      newSubcategory: undefined,
    };
    this.logout = this.logout.bind(this);
    this.student = this.student.bind(this);
    this.client = this.client.bind(this);
    this.addOffer = this.addOffer.bind(this);
    this.passCategory = this.passCategory.bind(this);
    this.passSubcategory = this.passSubcategory.bind(this);
    this.passContact = this.passContact.bind(this);
  }

  student() {
    localStorage.setItem('user', 'student');
    this.setState({ user: { student: localStorage.getItem('user') } });
  }

  client() {
    localStorage.setItem('user', 'client');
    this.setState({ user: { client: localStorage.getItem('user') } });
  }

  logout() {
    localStorage.removeItem('user');
    this.setState({ user: { student: undefined, client: undefined } });
  }

  passCategory(event) {
    this.setState({ newCategory: event.target.id });
  }

  passSubcategory(event) {
    this.setState({ newSubcategory: event.target.id });
  }

  passContact(event) {
    this.setState({ newContact: event.target.id });
  }


  addOffer() {
    const stateArray = this.state.offers.slice();
    const data = {
      category: this.state.newCategory,
      subcategory: this.state.newSubcategory,
      contact: this.state.newContact,
    };
    stateArray.push(data);
    this.setState({ offers: stateArray });
  }

  render() {
    const { children } = this.props;
    const childProps = {
      student: this.student,
      client: this.client,
      addOffer: this.addOffer,
      offers: this.state.offers,
      passCategory: this.passCategory,
      passSubcategory: this.passSubcategory,
      passContact: this.passContact,
    };

    const childrenWithProps = React.Children.map(children, (child) => {
      if (React.isValidElement(child)) return React.cloneElement(child, childProps);
      return child;
    });

    const content = childrenWithProps;

    return (
      <div className="wrapper">
        <div className="content">
          <Header
            student={this.state.user.student}
            client={this.state.user.client}
            logout={this.logout}
          />
          <main className={this.props.className}>
            {content}
          </main>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Layout;
