import React from 'react';
import { Link } from 'react-router-dom';
import './FirstLogin.css';

const FirstLogin = props => (
  <div className="first_login__container">
    <h1 className="h1__text">Wybierz rodzaj konta</h1>
    <div className="login__item">
      <Link href="/student/search" to="/student/search" className="button__login" onClick={props.student}>
        <span className="button__text">Student</span>
      </Link>
    </div>
    <div className="login__item">
      <Link href="/client/profile" to="/client/profile" className="button__login" onClick={props.client}>
        <span className="button__text">Klient</span>
      </Link>
    </div>
  </div>
);

export default FirstLogin;
