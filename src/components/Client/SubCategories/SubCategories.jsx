import React from 'react';
import { Link } from 'react-router-dom';
import './SubCategories.css';

const SubCategories = props => (
  <div className="subcategory__container">
    <h1 className="h1__text">Jaki problem występuje z komputerem</h1>
    <div className="subcategory__item">
      <Link href="/client/offers/create/ostel" to="/client/offers/create/ostel" className="button__subcategory" id="Nie włącza się" onClick={props.passSubcategory}>
        <span className="button-subcategory__text" id="Nie włącza się">Nie włącza się</span>
      </Link>
    </div>
    <div className="subcategory__item">
      <Link href="/client/offers/create/ostel" to="/client/offers/create/ostel" className="button__subcategory" id="Wolno działa" onClick={props.passSubcategory}>
        <span className="button-subcategory__text" id="Wolno działa">Wolno działa</span>
      </Link>
    </div>
    <div className="subcategory__item">
      <Link href="/client/offers/create/ostel" to="/client/offers/create/ostel" className="button__subcategory" id="Nie mogę zainstalować programu" onClick={props.passSubcategory}>
        <span className="button-subcategory__text" id="Nie mogę zainstalować programu">Nie mogę zainstalować programu</span>
      </Link>
    </div>
    <div className="subcategory__item">
      <Link href="/client/offers/create/ostel" to="/client/offers/create/ostel" className="button__subcategory" id="Zginęły pliki" onClick={props.passSubcategory}>
        <span className="button-subcategory__text" id="Zginęły pliki">Zginęły pliki</span>
      </Link>
    </div>
    <div className="subcategory__item">
      <Link href="/client/offers/create/other" to="/client/offers/create/other" className="button__subcategory" id="Inny" onClick={props.passSubcategory}>
        <span className="button-subcategory__text" id="Inny">Inny</span>
      </Link>
    </div>
  </div>
);

export default SubCategories;
