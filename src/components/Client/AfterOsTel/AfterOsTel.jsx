import React from 'react';
import { Link } from 'react-router-dom';
import './AfterOsTel.css';

class AfterOsTel extends React.Component {
  constructor(props) {
    super(props);
    this.addOffer = this.addOffer.bind(this);
  }

  addOffer() {
    const data = {
      category: localStorage.getItem('category'),
      subcategory: localStorage.getItem('subcategory'),
      contact: localStorage.getItem('contact'),
    };
    this.props.addOffer(data);
  }

  render() {
    return (
      <div className="login">
        <h1 className="h1__text">Wprowadz godziny kontaktu i płatności</h1>
        <div className="login-input__wrapper">
          <label className="offer__label" htmlFor="offer-from">Od</label>
          <input name="offer-from" required placeholder="8:00" className="offer__input" />
        </div>
        <div className="login-input__wrapper">
          <label className="offer__label" htmlFor="offer-to">Do</label>
          <input name="offer-to" required placeholder="14:00" className="offer__input" />
        </div>
        <div className="login-input__wrapper">
          <label className="offer__label" htmlFor="offer-cash">Oferta Płatności w zł</label>
          <input name="offer-cash" required placeholder="oferta" className="offer__input" />
        </div>
        <div className="button__wrapper">
          <Link href="/client/offers" to="/client/offers" className="offer__link" onClick={this.addOffer}>Stwórz</Link>
        </div>
      </div>
    );
  }
}

export default AfterOsTel;
