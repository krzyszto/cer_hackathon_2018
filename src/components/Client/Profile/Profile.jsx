import React from 'react';
import { Link } from 'react-router-dom';
import './Profile.css';

const Profile = () => (
  <div className="profile__container">
    <h2 className="h2__text">Twój profil</h2>
    <div className="input__wrapper">
      <label className="profile__label">Telefon</label>
      <input type="phone" name="phone" required placeholder="wpisz telefon kontaktowy" className="profile__input" />
    </div>
    <div>
      <h3 className="h3__text">Adres:</h3>
      <div className="input__wrapper">
        <label className="profile__label">Ulica i numer mieszkania</label>
        <input name="street" required placeholder="wpisz ulicę i numer mieszkania" className="login__input" />
      </div>
      <div className="input__wrapper">
        <label className="profile__label">Miasto</label>
        <input name="city" required placeholder="wpisz miasto" className="login__input" />
      </div>
    </div>
    <div>
      <h3 className="h3__text">Karta kredytowa:</h3>
      <div className="input__wrapper">
        <label className="profile__label">Numer karty</label>
        <input name="street" required placeholder="wpisz numer karty płatniczej" className="login__input" />
      </div>
      <div className="input__wrapper">
        <label className="profile__label">Numer CCV</label>
        <input name="city" required placeholder="CCV" className="login__input" />
      </div>
      <Link href="/client/card" to="/client/card" className="profile__label">Jak znaleźć dane karty, kliknij TUTAJ</Link>
    </div>
    <div className="input__wrapper">
      <Link href="/client/categories" to="/client/categories" className="profile__link">Zapisz</Link>
    </div>
  </div>
);

export default Profile;
