import React from 'react';
import { Link } from 'react-router-dom';

class OneOffer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hideDiv: false,
    };
    this.addClass = this.addClass.bind(this);
  }

  addClass() {
    this.setState({ hideDiv: true });
  }

  render() {
    const extraClass = this.state.hideDiv ? 'hidden' : '';
    return (
      <div className="one-offer">
        <li key={this.props.offer.id} className={`offer-result__item link__result ${extraClass}`}>
          <span className="category__wrapper"><img src={`/assets/${this.props.offer.category}.png`} alt="" className="result__icon" /><span>{this.props.offer.category}</span></span>
          <span className="offer-category__text2">{this.props.offer.subcategory}</span>
          <button className="cancel__button" onClick={this.addClass}>Anuluj</button>
          <Link href="/client/offers/completed" to="/client/offers/completed" className="accept__button">
            <button className="result__button">Potwierdź wykonanie</button>
          </Link>
        </li>
      </div>
    );
  }
}

export default OneOffer;
