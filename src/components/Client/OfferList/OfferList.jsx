import React from 'react';
import { Link } from 'react-router-dom';
import OneOffer from './OneOffer';
import './OfferList.css';

const OfferList = props => (
  <div className="offer-list">
    <h1 className="offer__header">Lista moich zgłoszeń</h1>
    <div className="offer-list__wrapper">
      <ul className="offer-result__list">
        {props.offers.map(offer => (
          <OneOffer offer={offer} />))}
      </ul>
      <div className="offer-link__wrapper">
        <Link href="/client/categories" to="/client/categories" className="button__offer">Wróć do kategorii</Link>
      </div>
    </div>
  </div>
);

export default OfferList;
