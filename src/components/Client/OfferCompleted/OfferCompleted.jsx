import React from 'react';
import { Link } from 'react-router-dom';
import './OfferCompleted.css';

const OfferCompleted = () => (
  <div className="offer-completed">
    <h2 className="h2__text">Wystaw ocenę</h2>
    <div className="star__wrapper2">
      <img src="/assets/star.png" alt="star" className="star__icon" />
      <img src="/assets/star.png" alt="star" className="star__icon" />
      <img src="/assets/star.png" alt="star" className="star__icon" />
      <img src="/assets/star.png" alt="star" className="star__icon" />
      <img src="/assets/star.png" alt="star" className="star__icon" />
    </div>
    <div className="input__wrapper">
      <label className="offer-completed__label" htmlFor="hours">Ilość godzin pracy</label>
      <input required placeholder="1" className="offer-completed__input" name="hours" />
    </div>
    <div className="input__wrapper">
      <Link href="/client/offers" to="/client/offers" className="offer_completed__link">Zapłać</Link>
    </div>
  </div>
);

export default OfferCompleted;
