import React from 'react';
import { Link } from 'react-router-dom';
import './Card.css';

const Card = () => (
  <div className="card__container">
    <h1 className="h1__text">Jak znaleźć dane karty</h1>
    <div className="card_wrapper"><img src="/assets/karta.png" alt="card" className="card__image" id="card" /></div>
    <div className="input__wrapper">
      <Link href="/client/profile" to="/client/profile" className="profile__link">Wróć</Link>
    </div>
  </div>
);

export default Card;
