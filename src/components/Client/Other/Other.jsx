import React from 'react';
import { Link } from 'react-router-dom';
import './Other.css';

const Other = () => (
  <div className="other__container">
    <h2 className="h2__text">Opisz swój problem z komputerem</h2>
    <div className="textarea__wrapper">
      <textarea className="other__textarea">
      </textarea>
    </div>

    <Link href="/client/offers/create/ostel" to="/client/offers/create/ostel" className="profile__link">Zatwierdź</Link>
  </div>
);

export default Other;
