import React from 'react';
import { Link } from 'react-router-dom';
import './OsTel.css';

const OsTel = props => (
  <div className="ostel__container">
    <h1 className="h1__text">Wybierz rodzaj pomocy</h1>
    <div className="ostel__item">
      <Link href="/client/offers/create/afterostel" to="/client/offers/create/afterostel" className="button__ostel" id="dom" onClick={props.passContact}>
        <img src="/assets/dom.png" alt="ikona dom" className="ostel__icon" id="dom" /><span id="dom">Wizyta domowa</span>
      </Link>
    </div>
    <div className="ostel__item">
      <Link href="/client/offers/create/afterostel" to="/client/offers/create/afterostel" className="button__ostel" id="telefon" onClick={props.passContact}>
        <img src="/assets/telefon.png" alt="telephone" className="ostel__icon" id="telefon" /><span id="telefon">Pomoc telefoniczna</span>
      </Link>
    </div>
  </div>
);

export default OsTel;
