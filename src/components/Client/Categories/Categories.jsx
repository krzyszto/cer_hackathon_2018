import React from 'react';
import { Link } from 'react-router-dom';
import './Categories.css';

const Categories = props => (
  <div className="category__container">
    <h1 className="h1__text">Wybierz kategorię problemu</h1>
    <div className="category__item">
      <Link href="/client/subcategories" to="/client/subcategories" className="button__category" id="komputer" onClick={props.passCategory}>
        <img src="/assets/komputer.png" alt="computer" className="category__icon" id="komputer" /><span className="category__text" id="komputer">Komputery</span>
      </Link>
    </div>
    <div className="category__item">
      <Link href="/client/subcategories" to="/client/subcategories" className="button__category" id="strona www" onClick={props.passCategory}>
        <img src="/assets/strona www.png" alt="web" className="category__icon" id="strona www" /><span className="category__text" id="strona www">Strony www</span>
      </Link>
    </div>
    <div className="category__item">
      <Link href="/client/subcategories" to="/client/subcategories" className="button__category" id="telefon" onClick={props.passCategory}>
        <img src="/assets/phone.png" alt="phone" className="category__icon" id="telefon" /><span className="category__text" id="telefon">Telefon</span>
      </Link>
    </div>
    <div className="category__item">
      <Link href="/client/subcategories" to="/client/subcategories" className="button__category" id="tv" onClick={props.passCategory}>
        <img src="/assets/tv.png" alt="tv" className="category__icon" id="tv" /><span className="category__text" id="tv">RTV</span>
      </Link>
    </div>
    <div className="category__item">
      <Link href="/client/subcategories" to="/client/subcategories" className="button__category" id="internet" onClick={props.passCategory}>
        <img src="/assets/internet.png" alt="internet" className="category__icon" id="internet" /><span className="category__text" id="internet">Internet</span>
      </Link>
    </div>
    <div className="category__item">
      <Link href="/client/subcategories" to="/client/subcategories" className="button__category" id="agd" onClick={props.passCategory}>
        <img src="/assets/agd.png" alt="agd" className="category__icon" id="agd" /><span className="category__text" id="agd">AGD</span>
      </Link>
    </div>
    <div className="other__item">
      <Link href="/client/profile" to="/client/profile" className="button__category">
        <span className="category__text">Profil</span>
      </Link>
    </div>
    <div className="other__item">
      <Link href="/client/offers" to="/client/offers" className="button__category">
        <span className="category__text">Moje Oferty</span>
      </Link>
    </div>
  </div>
);

export default Categories;
