import React from 'react';
import { Link } from 'react-router-dom';
import FirstLogin from 'components/FirstLogin/FirstLogin';

class FirstLoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <div>
        <FirstLogin student={this.props.student} client={this.props.client} />
      </div>
    );
  }
}

export default FirstLoginScreen;
