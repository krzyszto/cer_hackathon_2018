import React from 'react';
import MyOffers from 'components/Student/MyOffers/MyOffers';

class MyOffersScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <div>
        <MyOffers offers={this.props.offers} />
      </div>
    );
  }
}

export default MyOffersScreen;
