import React from 'react';
import { Redirect } from 'react-router';
import Search from 'components/Student/Search/Search';

class SearchScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      fireRedirect: false,
    };
    this.onChange = this.onChange.bind(this);
  }

  onChange() {
    this.setState({ fireRedirect: true });
  }

  render() {
    if (this.state.fireRedirect) return <Redirect to="/" />;
    return (
      <div>
        <Search onChange={this.onChange} offers={this.props.offers} />
      </div>
    );
  }
}

export default SearchScreen;
