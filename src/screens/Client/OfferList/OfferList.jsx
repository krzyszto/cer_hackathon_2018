import React from 'react';
import OfferList from 'components/Client/OfferList/OfferList';

class OfferListScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <div>
        <OfferList offers={this.props.offers} />
      </div>
    );
  }
}

export default OfferListScreen;
