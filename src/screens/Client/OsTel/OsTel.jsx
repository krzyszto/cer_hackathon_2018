import React from 'react';
import OsTel from 'components/Client/OsTel/OsTel';

class OsTelScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <div>
        <OsTel passContact={this.props.passContact} />
      </div>
    );
  }
}

export default OsTelScreen;
