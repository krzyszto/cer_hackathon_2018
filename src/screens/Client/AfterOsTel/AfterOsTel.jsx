import React from 'react';
import AfterOsTel from 'components/Client/AfterOsTel/AfterOsTel';

class AfterOsTelScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <div>
        <AfterOsTel addOffer={this.props.addOffer} />
      </div>
    );
  }
}

export default AfterOsTelScreen;
