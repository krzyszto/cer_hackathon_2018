import React from 'react';
import { Link } from 'react-router-dom';
import Categories from 'components/Client/Categories/Categories';

class CategoriesScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <div>
        <Categories passCategory={this.props.passCategory} />
      </div>
    );
  }
}

export default CategoriesScreen;
