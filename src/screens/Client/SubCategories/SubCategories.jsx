import React from 'react';
import { Link } from 'react-router-dom';
import SubCategories from 'components/Client/SubCategories/SubCategories';

class SubCategoriesScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <div>
        <SubCategories passSubcategory={this.props.passSubcategory} />
      </div>
    );
  }
}

export default SubCategoriesScreen;
