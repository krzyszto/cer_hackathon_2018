import React from 'react';
import ReactDOM from 'react-dom';
import App from 'core/App';
import './index.css';

ReactDOM.render(
  React.createElement(App),
  document.getElementById('root')
);
